const cors       =   require('cors');
const bodyParser =   require('body-parser');
const express = require('express');
const multer = require('multer');
const upload = multer();

const user = require('../routes/auth.router');
const openApi = require('../routes/open-api.router');
const ticket_reports = require('../routes/ticket_reports.router');
const response = require('../routes/response.router');
const compartment = require('../routes/compartment.router');
const beat = require('../routes/beat.router');
const block = require('../routes/block.router');
const range = require('../routes/range.router');
const division = require('../routes/division.router');
const circle = require('../routes/circle.router');
const jwt   = require('jsonwebtoken');

module.exports = (app) => {
    // for cors 
    app.use(cors());

    // for parsing application/json
    app.use(bodyParser.json({limit: '50mb'}));

    // for parsing application/xwww-form-urlencoded
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

    // for parsing multipart/form-data
    // app.use(upload.array()); 
    app.use(express.static(__dirname + '/public'));
    //app.use(express.static('public'));

    app.use(openApi);
    app.use(jwtVerify);
    app.use(ticket_reports);
    app.use(response);
    app.use(user);
    app.use(compartment);
    app.use(beat);
    app.use(block);
    app.use(range);
    app.use(division);
    app.use(circle);

   

   // JWT Authentication
   function jwtVerify (req, res, next) {

    if( typeof req.headers.authorization !== 'undefined') {

      let token = req.headers.authorization;
      const authToken = token.split(" ")[1];

      jwt.verify(authToken, 'ankur47463', (err) => {
            if(err) {
                res.sendStatus(401);
                next(err);
            } else {
                next();
            }
        });       
    } else {
        res.sendStatus(401);
    }
  }
}