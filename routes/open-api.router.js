const auth = require('../models/auth.model');
const response = require('../models/response.model');
const express = require('express');
const router = express.Router();


router.route('/secure_api/user/authentication').post(userAuthentication);
router.route('/secure_api/image/:filename').get(getImage);

function userAuthentication(req,res) {
  auth.userAuthentication(req.body).then((data) => {
    if(data) {
      res.json(data);
    }
    }).catch((err) => {
      res.json( {"success":false,
                 "message":"An Error Occurred...Server Not Responding..!!"}
              )
  });
} 

function getImage(req,res) {
  response.getImage(req, res)
  // .then((data) => {
  //   if(data) {
  //     res.json(data);
  //   }
  //   }).catch((err) => {
  //     res.json( {"success":false,
  //                "message":"An Error Occurred...Server Not Responding..!!"}
  //             )
  // });
} 



module.exports = router;
