const circle = require('../models/circle.model');
const express = require('express');
const router = express.Router();

router.route('/secure_api/circle/fetch').get(getAllCircle);


function getAllCircle(req,res) {
    circle.getAllCircle(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
   });
} 


module.exports = router;