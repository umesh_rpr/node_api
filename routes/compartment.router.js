const compartment = require('../models/compartment.model');
const express = require('express');
const router = express.Router();

router.route('/secure_api/compartment/fetch').get(getAllCompartment);


function getAllCompartment(req,res) {
    compartment.getAllCompartment(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
   });
} 


module.exports = router;