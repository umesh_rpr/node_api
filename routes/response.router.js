const response = require('../models/response.model');
const express = require('express');
const router = express.Router();
const { singleFileUpload, uploadTextOnly } = require('../models/response.model');

router.route('/secure_api/response/create').put(createResponse);
router.route('/secure_api/response/fetch/:level').get(getAllResponse);
router.route('/secure_api/response/update/:_id').post(singleFileUpload, updateResponse);
router.route('/secure_api/response/reply/:_id').post(uploadTextOnly, updateReply);


function createResponse(req, res) {
    response.createResponse(req).then((data) => {
      if(data) {
        res.json(data);
      }
      }).catch((err) => {
        res.json( {"success":false,"message":"An Error Occurred...Server Not Responding..!!"})
    });
} 


function getAllResponse(req,res) {
    response.getAllResponse(req).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
   });
} 


function updateResponse(req, res) {
  console.log("req.body in response",req.body);
  response.updateResponse(req, res).then((data) => {
      if(data) {
      res.json(data);
      }
  }).catch((err) => {
      res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
  });
} 


function updateReply(req, res) {
  console.log("req.body", req.body)
  response.updateReply(req).then((data) => {
      if(data) { res.json(data); }
  }).catch((err) => {
      res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
  });
} 

module.exports = router;