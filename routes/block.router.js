const block = require('../models/block.model');
const express = require('express');
const router = express.Router();

router.route('/secure_api/block/fetch').get(getAllBlock);


function getAllBlock(req,res) {
    block.getAllBlock(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
   });
} 


module.exports = router;