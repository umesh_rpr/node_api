const ticket_reports = require('../models/ticket_reports.model');
const express = require('express');
const router = express.Router();

router.route('/secure_api/ticket_reports/create').put(createTicketReports);
router.route('/secure_api/ticket_reports/fetch').get(getAllTicketReports);
router.route('/secure_api/ticket_reports/update/:_id').post(updateTicketReports);


  function createTicketReports(req,res) {
    ticket_reports.createTicketReports(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
      }).catch((err) => {
        console.log(err);
        res.json( {"success":false,
                   "message":"An Error Occurred...Server Not Responding..!!"}
                )
    });
  } 


  function getAllTicketReports(req,res) {
    ticket_reports.getAllTicketReports(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
      res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
    });
  } 


  function updateTicketReports(req, res) {
    ticket_reports.updateTicketReports(req).then((data) => {
        if(data) {
        res.json(data);
        }
    }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
    });
  } 

module.exports = router;

