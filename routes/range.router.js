const range = require('../models/range.model');
const express = require('express');
const router = express.Router();

router.route('/secure_api/range/fetch').get(getAllRange);


function getAllRange(req,res) {
    range.getAllRange(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
   });
} 


module.exports = router;