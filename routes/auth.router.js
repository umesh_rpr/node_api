const auth = require('../models/auth.model');
const express = require('express');
const router = express.Router();

router.route('/secure_api/user/create').put(createUser);
router.route('/secure_api/user/fetch').get(getAllUsers);
router.route('/secure_api/user/admin/fetch').get(getAdminUsers);
router.route('/secure_api/user/fetch/me').get(getSingleUser);
router.route('/secure_api/user/update/:_id').post(updateUser);


  function createUser(req,res) {
    auth.createUser(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
      }).catch((err) => {
        console.log(err);
        res.json( {"success":false,
                   "message":"An Error Occurred...Server Not Responding..!!"}
                )
    });
  } 


  function getAllUsers(req,res) {
    auth.getAllUsers(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
      //console.log(err);
      res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
    });
  } 


  function getAdminUsers(req,res) {
    auth.getAdminUsers(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
      console.log(err);
      res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
    });
  } 


  function updateUser(req, res) {
    auth.updateUser(req).then((data) => {
      if(data) {
        res.json(data);
      }
    }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
    });
  } 


  function getSingleUser(req, res) {
    auth.getSingleUser(req).then((data) => {
      if(data) {
        res.json(data);
      }
    }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
    });
  } 

module.exports = router;

