const beat = require('../models/beat.model');
const express = require('express');
const router = express.Router();

router.route('/secure_api/beat/fetch').get(getAllBeat);


function getAllBeat(req,res) {
    beat.getAllBeat(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
   });
} 


module.exports = router;