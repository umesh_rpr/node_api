const division = require('../models/division.model');
const express = require('express');
const router = express.Router();

router.route('/secure_api/division/fetch').get(getAllDivision);


function getAllDivision(req,res) {
    division.getAllDivision(req.body).then((data) => {
      if(data) {
        res.json(data);
      }
   }).catch((err) => {
        res.json({"success":false, "message":"An Error Occurred...Server Not Responding..!!"})
   });
} 


module.exports = router;