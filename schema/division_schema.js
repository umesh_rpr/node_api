const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Division Schema
const DivisionSchema = new Schema({

    name: { type: String, required: [true] },

    circle: { type: Schema.Types.ObjectId, ref: "circle", required: [true] },

});


module.exports = mongoose.model('division', DivisionSchema);