const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// User Schema
const UserSchema = new Schema({

    mobile: { type: String, required: [true] },

    name: { type: String, required: [true] },

    usertype: { type: String, enum: ["circle", "division", "range", "block", "beat", "compartment"], required: [true] },
    
    usertype_id: { type: Schema.Types.ObjectId, required: [true]},

    level: { type: Number, required: [true] },

});


module.exports = mongoose.model('users', UserSchema);