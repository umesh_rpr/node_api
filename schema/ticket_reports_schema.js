const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Ticket Report Schema
const TicketReportSchema = new Schema({

    fire_sno: { type: Number, required: [true] },

    fire_datetime: { type: Date, required: [true] },

    lat: { type: Number, required: [true] },

    lon: { type: Number, required: [true] },

    compartment_id: { type: String, required: [true] },

    status: { type: Boolean, required: [true], default: false },

    created_at: { type: Date, default: Date.now }

});


module.exports = mongoose.model('ticket_report', TicketReportSchema);