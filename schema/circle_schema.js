const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Circle Schema
const CircleSchema = new Schema({

    name: { type: String, required: [true] },

});


module.exports = mongoose.model('circle', CircleSchema);