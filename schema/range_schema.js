const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Range Schema
const RangeSchema = new Schema({

    name: { type: String, required: [true] },

    division: { type: Schema.Types.ObjectId, ref: "division", required: [true] },

});


module.exports = mongoose.model('range', RangeSchema);