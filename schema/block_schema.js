const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Block Schema
const BlockSchema = new Schema({

    name: { type: String, required: [true] },

    range: { type: Schema.Types.ObjectId, ref: "range", required: [true] },

});


module.exports = mongoose.model('block', BlockSchema);