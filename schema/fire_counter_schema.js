const mongoose = require('mongoose');

const fireCounterSchema = mongoose.Schema({
    _id: {type: String, required: true},
    seq: {type:Number, default: 10000},
});

module.exports = mongoose.model('fire_counter', fireCounterSchema);