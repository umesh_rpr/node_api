const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Response Schema
const ResponseSchema = new Schema({

    ticket_report: { type: Schema.Types.ObjectId, ref: "ticket_report", required: [true] },

    level: { type: Number, required: [true], default: 0 },
    
    reply: [ 
        {
            fire_found: { type: Boolean },

            proposal: { type: Boolean },

            reason_extinguish: { type: String },

            affected_area: { type: Number },

            assessed_amount: { type: Number },

            loss: { type: String },

            arn_area: { type: Boolean },

            plantation_area: { type: Boolean },

            remark: { type: String},

            lat: { type : String },

            lon: { type: String },

            photo: { type: String },

            created_by: { type: Schema.Types.ObjectId, ref: 'users' },

            created_at: { type: Date, default: Date.now }
        },
     ],
});


module.exports = mongoose.model('response', ResponseSchema);