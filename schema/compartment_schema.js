const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Compartment Schema
const CompartmentSchema = new Schema({

    name: { type: String, required: [true] },

    beat: { type: Schema.Types.ObjectId, ref: "beat", required: [true] },

});


module.exports = mongoose.model('compartment', CompartmentSchema);