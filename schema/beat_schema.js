const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Beat Schema
const BeatSchema = new Schema({

    name: { type: String, required: [true] },

    block: { type: Schema.Types.ObjectId, ref: "block", required: [true] },

});


module.exports = mongoose.model('beat', BeatSchema);