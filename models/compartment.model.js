const compartmentModel = require('../schema/compartment_schema');
const jwt       = require('jsonwebtoken');
var ObjectID    = require('mongodb').ObjectID;


// Get All Compartment
function getAllCompartment(body) {
  return new Promise((resolve, reject) => {
    compartmentModel.find(body)
    .then( result => {
      if(result.length > 0)
        resolve({"success": true, data: result});
      else
        resolve({"success": false, data: result});
    }).catch( err => {reject(err);}) 
  });
}

module.exports = { getAllCompartment };