const beatModel = require('../schema/beat_schema');
const jwt       = require('jsonwebtoken');
var ObjectID    = require('mongodb').ObjectID;


// Get All Beat
function getAllBeat(body) {
  return new Promise((resolve, reject) => {
    beatModel.find(body)
    .then( result => {
      if(result.length > 0)
        resolve({"success": true, data: result});
      else
        resolve({"success": false, data: result});
    }).catch( err => {reject(err);}) 
  });
}

module.exports = { getAllBeat };