const userModel = require('../schema/users_schema');
const jwt       = require('jsonwebtoken');
const otpGenerator = require('otp-generator');
const request  = require('request');
var ObjectID    = require('mongodb').ObjectID;


function userAuthentication(body) {
  return new Promise((resolve, reject) => {
    userModel.findOne( {'mobile': body.mobile} )
    .then( result => { 
      //console.log('result ', result);
      if(result._id) {

          const user_id = {
            "_id": result._id,
          }

          // GET TOKEN
          const token = jwt.sign({user: user_id}, 'ankur47463', { expiresIn: '15d' });

          // CREATE OTP
          const otp = otpGenerator.generate(6, {upperCase: false, specialChars: false, alphabets: false, digits: true});

          // SEND OTP
          const smsOtp = {otp: otp, mobile: result.mobile};
          //sendSMS(smsOtp);

          resolve({
            "success"    : true,
            "jwt"        : token,
            "otp"        : otp,
            "user_level" : result.user_level

          });
      }else {
        resolve({
            "success":false,
            "message":"Invalid Mobile Number."
        })
      }
    }).catch( err => {reject(err);});
  });
}


// Create New User
function createUser(body) {
  return new Promise((resolve, reject) => {
    userModel.create(body)
    .then( result => {
      if(result._id)
        resolve({"success": true, message: "User Created Successfully"});
      else
        resolve({"success": false, message: "User Creation Failed..!!"});
    }).catch( err => {reject(err);}) 
  });
}


// Get All Users
function getAllUsers(body) {
  return new Promise((resolve, reject) => {
    userModel.find(body)
    .then( result => {
      if(result.length > 0)
        resolve({"success": true, data: result});
      else
        resolve({"success": false, data: result});
    }).catch( err => {reject(err);}) 
  });
}


// Get All Users
// function getAdminUsers(body) {
//   return new Promise((resolve, reject) => {
//     wingModel.find({}, {admin_username: 1, _id: 0})
//     .then(res => {
//       let objArray =  res.map(a => a.admin_username);
//       userModel.find( {$and: [{usertype: "Admin"}, {username: {$nin: objArray }}]})
//       .then( result => {
//         if(result.length > 0)
//           resolve({"success": true, data: result});
//         else
//           resolve({"success": false, data: result});
//       }).catch( err => {reject(err);}) 
//     })
//   });
// }


// update User
function updateUser(req) {
  return new Promise((resolve, reject) => {
   // console.log(req.body, req.params._id);
    userModel.updateOne({"_id" : ObjectID(req.params._id)}, { $set: req.body } )
    .then( result => {
      if(result.n === 1 && result.nModified === 1, result.ok=== 1){
         resolve({"success": true, message: "User Updated Successfully."}); 
      } else {
        resolve({"success": false, message: "User Updation Failed."}); 
      } 
      }).catch( err => {reject(err);}) 
  });
}


// Get user Users
function getSingleUser(req) {
  let token = req.headers.authorization;
  const authToken = token.split(" ")[1];

  const decoded = jwt.verify(authToken, 'ankur47463');
  
  return new Promise((resolve, reject) => {
    userModel.findOne(decoded.user)
    .then( result => {
      if(result._id)
        resolve({"success": true, data: result});
      else
        resolve({"success": false, data: result});
    }).catch( err => {reject(err);}) 
  });
}

// SMS function
function sendSMS(data) {
 const message = `
 ${data.otp} Has Been OTP
For FTS Login.

If Login Issue Call Us: 8480000032
One Solution Raipur.`;
 
     var data =  [
                     {
                             message: message,
                             to: [
                                 data.mobile,
                             ]
                         }
                 ]
                 
     let formdata = { senderId: "ONESOL",
                     sms: JSON.stringify(data)
                 }
                 
     request.post({url:'http://www.aashiweb.in/msg91SendSMS.php', form: formdata}, function(err,response,resbody) {
         //console.log('statusCode:', response && response.statusCode, resbody);
     });
}


module.exports = { userAuthentication, createUser, getAllUsers, updateUser, getSingleUser };