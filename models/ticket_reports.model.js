const ticketReportsModel = require('../schema/ticket_reports_schema');
const fireCounterSchema = require('../schema/fire_counter_schema');
const jwt       = require('jsonwebtoken');
var ObjectID    = require('mongodb').ObjectID;


// Create New Ticket Reports
function createTicketReports(body) {
  return new Promise(async (resolve, reject) => {

    let fetch_new_fire_sno = await fetchNewFireNo();
    body.fire_sno = fetch_new_fire_sno.fire_sno;

    ticketReportsModel.create(body)
    .then( result => {
      if(result._id)
        resolve({"success": true, message: "Ticket Reports Created Successfully"});
      else
        resolve({"success": false, message: "Ticket Reports Creation Failed..!!"});
    }).catch( err => {reject(err);}) 
  });
}


// Get All Ticket Reports
function getAllTicketReports(body) {
  return new Promise((resolve, reject) => {
    console.log('body', body);
    ticketReportsModel.find(body)
    .then( result => {
      if(result.length > 0)
        resolve({"success": true, data: result});
      else
        resolve({"success": false, data: result});
    }).catch( err => {reject(err);}) 
  });
}


// Update Ticket Reports
function updateTicketReports(req) {
  return new Promise( (resolve, reject) => {
      //console.log(req.params._id, req.body);
      ticketReportsModel.updateOne({"_id" : ObjectID(req.params._id)}, { $set: req.body })
      .then( result => {
          if(result.n === 1 && result.nModified === 1, result.ok=== 1){
              resolve({"success": true, message: "Ticket Reports Updated Successfully."}); 
          } else {
              resolve({"success": false, message: "Ticket Reports Updation Failed."}); 
          } 
      }).catch( err => {reject(err);}) 
  });
}


// For Fire sno increment
function fetchNewFireNo(req,res){
  return new Promise(function(resolve, reject) {
    fireCounterSchema.findByIdAndUpdate({_id: 'entityId'}, {$inc: { seq: 1}}, { upsert: true, new: true})
    .then(async fire_counter => {  
        
      console.log('yahhh', fire_counter);   
      if(fire_counter._id){
        resolve({"fire_sno": fire_counter.seq});
      }else{
        resolve({"fire_sno":10001});
      }
    })
  })
}


module.exports = { createTicketReports, getAllTicketReports, updateTicketReports };