const rangeModel = require('../schema/range_schema');
const jwt       = require('jsonwebtoken');
var ObjectID    = require('mongodb').ObjectID;


// Get All Range
function getAllRange(body) {
  return new Promise((resolve, reject) => {
    rangeModel.find(body)
    .then( result => {
      if(result.length > 0)
        resolve({"success": true, data: result});
      else
        resolve({"success": false, data: result});
    }).catch( err => {reject(err);}) 
  });
}

module.exports = { getAllRange };