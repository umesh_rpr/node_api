const divisionModel = require('../schema/division_schema');
const jwt       = require('jsonwebtoken');
var ObjectID    = require('mongodb').ObjectID;


// Get All Division
function getAllDivision(body) {
  return new Promise((resolve, reject) => {
    divisionModel.find(body)
    .then( result => {
      if(result.length > 0)
        resolve({"success": true, data: result});
      else
        resolve({"success": false, data: result});
    }).catch( err => {reject(err);}) 
  });
}

module.exports = { getAllDivision };