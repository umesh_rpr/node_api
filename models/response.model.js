const responseModel = require('../schema/response_schema');
var ObjectID    = require('mongodb').ObjectID;
const crypto = require("crypto");
var multer  = require('multer');
const GridFsStorage = require("multer-gridfs-storage");
const { mongoURI } = require('../config/db');
const path = require("path");
const mongoose = require('mongoose');

// Storage
const storage = new GridFsStorage({
  url: mongoURI,
  options: {useUnifiedTopology: true},
  cache: true,
  file: (req, file) => {
    return new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const filename = buf.toString("hex") + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          bucketName: "uploads"
        };
        resolve(fileInfo);
      });
    });
  }
});

// Validate for file filter
const fileFilter = (req, file, cb) => {
  if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  }else {
    cb(null, false);
  }
}
 
// Set Storage To Multer
const upload = multer({storage, fileFilter: fileFilter});

// Accept To Geting Single File
const singleFileUpload = upload.single('file');


// Get Image
function getImage(req, res) {
  let gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {bucketName: "uploads"});
  //console.log("/image/:filename", req.params.filename);
  gfs.find({filename: req.params.filename})
   .toArray((err, files) => {
     if (!files || files.length === 0) {
       return res.status(404).json({
         err: "no files exist"
       });
     }
   gfs.openDownloadStreamByName(req.params.filename).pipe(res);
  
   });
}


// Create New Response
function createResponse(body) {
  return new Promise((resolve, reject) => {
    responseModel.create(body)
    .then( result => {
      if(result._id)
        resolve({"success": true, message: "Response Created Successfully"});
      else
        resolve({"success": false, message: "Response Creation Failed..!!"});
    }).catch( err => {reject(err);}) 
  });
}


// Get All Responses
function getAllResponse(req) {
  promise1 = responseModel.find({ "level": {$lt : req.params.level}})
  .populate('ticket_report')
  .populate('reply.created_by', 'name')
  .sort({_id:-1})
  .then( result => {
      return result;
  });

  promise2 = responseModel.find({ "level": {$eq : req.params.level}})
    .populate('ticket_report')
    .populate('reply.created_by', 'name')
    .sort({_id:-1})
    .then( result => {
        return result;
    });



  return  Promise.all([promise1, promise2])
    .then((result) => Promise.resolve({success: true, data: result}) )
  
}

// // Get All Responses
// function getAllResponse(body) {
//   return new Promise((resolve, reject) => {
//     responseModel.find({reply: { $exists: true, $ne: [] }})
//     .populate('ticket_report')
//     .sort({_id:-1})
//     .then( result => {
//       if(result.length > 0)
//         resolve({"success": true, data: result});
//       else
//         resolve({"success": false, data: result});
//     }).catch( err => {reject(err);}) 
//   });
// }


// Update Response
function updateResponse(req, res) {
  return new Promise( (resolve, reject) => {
      //console.log(req.params._id, req.body);
      //res.json({ file: req.file });
    const reply_data = {
          fire_found: req.body.fire_found,
          proposal: req.body.proposal,
          reason_extinguish: req.body.reason_extinguish,
          affected_area: req.body.affected_area,
          assessed_amount: req.body.assessed_amount,
          loss: req.body.loss,
          arn_area: req.body.arn_area,
          plantation_area: req.body.plantation_area,
          remark: req.body.remark,
          lat: req.body.lat,
          lon: req.body.lon,
          photo: req.file.filename,
          created_by: req.body.created_by
    };
  
  const data = { 
    $set: { level: req.body.level },
    $push: { reply: reply_data } 
  }

  //console.log(data, req.params._id);

      responseModel.updateOne({"_id" : ObjectID(req.params._id)}, data )
      .then( result => {
          if(result.n === 1 && result.nModified === 1, result.ok=== 1){
              resolve({"success": true, message: "Response Updated Successfully."}); 
          } else {
              resolve({"success": false, message: "Response Updation Failed."}); 
          } 
      }).catch( err => {reject(err);}) 
  });
}


// Node accept Form-data via multer only as of now
const uploadMulter = multer();

const uploadTextOnly = uploadMulter.none();

// Update Response
function updateReply(req) {
  return new Promise( (resolve, reject) => {
    const reply_data = {
          proposal: req.body.proposal,
          remark: req.body.remark,
          created_by: req.body.created_by
    };

    const data = { 
      $set: { level: req.body.level },
      $push: { reply: reply_data } 
    }

    console.log("updateReply", data);
      responseModel.updateOne({"_id" : ObjectID(req.params._id)}, data )
      .then( result => {
          if(result.n === 1 && result.nModified === 1, result.ok=== 1){
              resolve({"success": true, message: "Response Updated Successfully."}); 
          } else {
              resolve({"success": false, message: "Response Updation Failed."}); 
          } 
      }).catch( err => {reject(err);}) 
  });
}
module.exports = { createResponse, getAllResponse, updateResponse, updateReply, singleFileUpload, uploadTextOnly, getImage };