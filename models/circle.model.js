const circleModel = require('../schema/circle_schema');
const jwt       = require('jsonwebtoken');
var ObjectID    = require('mongodb').ObjectID;


// Get All Circle
function getAllCircle(body) {
  return new Promise((resolve, reject) => {
    circleModel.find(body)
    .then( result => {
      if(result.length > 0)
        resolve({"success": true, data: result});
      else
        resolve({"success": false, data: result});
    }).catch( err => {reject(err);}) 
  });
}

module.exports = { getAllCircle };