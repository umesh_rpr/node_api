const mongoose = require('mongoose');
const fireCounterModel = require('../schema/fire_counter_schema');
const userModel = require('../schema/users_schema');

const mongoURI = "mongodb://localhost/fire_transmission_system";

// Establish Mongodb Connection
mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true })
.then(() => console.log('MongoDB Connection Established!!'))
.catch(err => console.error('MongoDB Connection Failed!!', err));

// init gfs
mongoose.connection.once('open', () => {
  // init stream
  new mongoose.mongo.GridFSBucket(mongoose.connection.db, {bucketName: "uploads"});
});


// Create Fire Counter
fireCounterModel.findOneAndUpdate({"_id": "entityId"}, {$setOnInsert:{"_id": "entityId", "seq": 10000}}, { upsert: true, new: true })
.then((res) => { console.log('Fire Counter Created!!')});


// Create Default Super User
userModel.findOneAndUpdate({"created_by": "5e75ab99afcc3e044b277b76"},
 {$setOnInsert: {
     "name": "One Solution",
     "mobile": "8463840280",
     "user_level": "Circle",
     "created_by": "5e75ab99afcc3e044b277b76"
    }
}, { upsert: true})
.then((res) => { console.log('Default User Created!')});


module.exports = { mongoURI };
