const express  =  require("express");
const app      =  express();

// Connect To Mongodb
require('./config/db');
require('./startup/routes')(app);

// Server Listen On PORT 3000
const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log("App Started...Server listening at PORT: "+PORT);
});
