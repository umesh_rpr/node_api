module.exports = {
  apps : [{
    name        : "Shyam Industries",
    script      : "app.js",
    watch       : true,
    merge_logs  : true,
    cwd         : "/root/secure_api/",
   }]
}